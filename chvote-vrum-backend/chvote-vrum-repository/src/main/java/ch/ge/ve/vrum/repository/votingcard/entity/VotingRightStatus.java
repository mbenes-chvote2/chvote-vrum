/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.repository.votingcard.entity;

/**
 * An enumeration with all the possible right status of a {@link VotingCard}.
 */
public enum VotingRightStatus {
  /**
   * The {@link VotingCard} can still be used to vote: it has not yet been used or its right to use it has been
   * restored.
   */
  AVAILABLE,
  /**
   * The {@link VotingCard} is currently in use on the electronic voting system. A confirmation code has been provided
   * to the user but it has not yet been acknowledged.
   */
  LOCKED,
  /**
   * The {@link VotingCard} has been used.
   */
  USED,
  /**
   * An authorized VRUM user has blocked the {@link VotingCard}.
   */
  BLOCKED
}
