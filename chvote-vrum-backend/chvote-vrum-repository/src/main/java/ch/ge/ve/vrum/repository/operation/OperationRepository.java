/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.repository.operation;

import ch.ge.ve.vrum.repository.operation.entity.Operation;
import java.util.Optional;
import java.util.stream.Stream;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The {@link Operation} repository.
 */
@Repository
public interface OperationRepository extends JpaRepository<Operation, Long> {
  /**
   * Find a {@link Operation} by {@link Operation#protocolId}.
   *
   * @param protocolId the {@link Operation#protocolId}.
   *
   * @return An {@link Optional} containing the matching {@link Operation} if it exists.
   */
  Optional<Operation> findByProtocolId(String protocolId);

  /**
   * Find all {@link Operation} entities matching the specified test property.
   *
   * @param test whether the {@link Operation} is in test.
   *
   * @return the stream with all the matching {@link Operation} entities.
   */
  Stream<Operation> findByTest(boolean test);
}
