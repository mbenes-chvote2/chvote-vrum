/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.mock.server.service;

import ch.ge.ve.vrum.repository.votingcard.DomainOfInfluenceRepository;
import ch.ge.ve.vrum.repository.votingcard.entity.DomainOfInfluence;
import com.google.common.collect.ImmutableMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * A service that creates and persists mocked {@link DomainOfInfluence} entities.
 */
@Service
public class DomainOfInfluenceService {
  private static final Map<String, String> DOMAIN_OF_INFLUENCES = ImmutableMap.of(
      "GE", "Genève"
  );

  private final DomainOfInfluenceRepository domainOfInfluenceRepository;

  /**
   * Create a new domain of influence service.
   *
   * @param domainOfInfluenceRepository the {@link DomainOfInfluenceRepository}.
   */
  @Autowired
  public DomainOfInfluenceService(DomainOfInfluenceRepository domainOfInfluenceRepository) {
    this.domainOfInfluenceRepository = domainOfInfluenceRepository;
  }

  /**
   * Create and persist a set of predefined {@link DomainOfInfluence} entities:
   *
   * <h3 id="predefined">Predefined Domain of Influences</h3>
   * <table class="striped" style="text-align:left">
   * <thead>
   * <tr>
   * <th scope="col">businessId</th>
   * <th scope="col">name</th>
   * </tr>
   * </thead>
   * <tbody>
   * <tr>
   * <th scope="row">GE</th>
   * <td>Genève</td>
   * </tr>
   * </tbody>
   * </table>
   */
  @Transactional
  public void createAll() {
    DOMAIN_OF_INFLUENCES.forEach((businessId, name) -> {
      DomainOfInfluence domainOfInfluence = new DomainOfInfluence();
      domainOfInfluence.setBusinessId(businessId);
      domainOfInfluence.setName(name);
      domainOfInfluenceRepository.save(domainOfInfluence);
    });
  }

  /**
   * Delete all the persisted entities regardless that they were created by this service.
   */
  @Transactional
  public void deleteAll() {
    domainOfInfluenceRepository.deleteAll();
  }
}
