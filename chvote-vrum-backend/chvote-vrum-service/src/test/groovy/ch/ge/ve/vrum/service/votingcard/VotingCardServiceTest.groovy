/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.service.votingcard

import ch.ge.ve.vrum.repository.operation.OperationRepository
import ch.ge.ve.vrum.repository.operation.entity.Operation
import ch.ge.ve.vrum.repository.votingcard.VotingCardRepository
import ch.ge.ve.vrum.repository.votingcard.entity.CountingCircle
import ch.ge.ve.vrum.repository.votingcard.entity.DomainOfInfluence
import ch.ge.ve.vrum.repository.votingcard.entity.VotingCard
import ch.ge.ve.vrum.repository.votingcard.entity.VotingChannel
import ch.ge.ve.vrum.repository.votingcard.entity.VotingRightStatus
import ch.ge.ve.vrum.service.exception.EntityNotFoundException
import ch.ge.ve.vrum.service.exception.IllegalVotingRightStatusTransitionException
import java.time.LocalDateTime
import spock.lang.Specification

class VotingCardServiceTest extends Specification {

  private VotingCardRepository votingCardRepository = Mock(VotingCardRepository)
  private OperationRepository operationRepository = Mock(OperationRepository)
  private VotingCardService service = new VotingCardService(votingCardRepository, operationRepository)

  def "findByOperationIdAndVoterIndex should return all voting cards matching the given operationId and voterIndex"() {
    def votingCardStub = createVotingCard(1, 10)

    given:
    operationRepository.findById(_) >> Optional.of(votingCardStub.operation)
    votingCardRepository.findByOperation_IdAndVoterIndex(6, 1) >> Optional.of(votingCardStub)

    when:
    def votingCard = service.findByOperationIdAndVoterIndex(6, 1).get()

    then:
    votingCard != null
    votingCard.id == 10
  }

  def "findByOperationIdAndVoterIndex should throw an exception if no Operation corresponds to the given parameters"() {
    given:
    operationRepository.findById(_) >> Optional.empty()
    votingCardRepository.findByOperation_IdAndVoterIndex(6, 1) >> Optional.empty()

    when:
    service.findByOperationIdAndVoterIndex(6, 1)

    then:
    thrown(EntityNotFoundException)
  }

  def "markAsUsed should update the card's voting right status to USED"() {
    def votingCard = createVotingCard(1, 10)

    given:
    operationRepository.findById(_) >> Optional.of(votingCard.operation)
    votingCardRepository.findByOperation_IdAndVoterIndex(6, 1) >> Optional.of(votingCard)
    votingCardRepository.save(_) >> votingCard

    when:
    service.markAsUsed(6, 1, VotingChannel.MAIL)
    votingCard = service.findByOperationIdAndVoterIndex(6, 1).get()

    then:
    votingCard.id == 10
    votingCard.votingRightStatus == VotingRightStatus.USED
    votingCard.votingChannel == VotingChannel.MAIL
    votingCard.votingDatetime != null
  }

  def "markAsUsed should throw a VotingRightStatusException when the card's voting right status doesn't allow a status update"() {
    def votingCard = createVotingCard(1, 10)
    votingCard.votingRightStatus = votingRightStatus

    given:
    votingCardRepository.findByOperation_IdAndVoterIndex(6, 1) >> Optional.of(votingCard)

    when:
    service.markAsUsed(6, 1, VotingChannel.MAIL)

    then:
    thrown(IllegalVotingRightStatusTransitionException)

    where:
    votingRightStatus << [
            VotingRightStatus.USED,
            VotingRightStatus.BLOCKED,
            VotingRightStatus.LOCKED
    ]
  }

  def "markAsUsed should throw an IllegalArgumentException when the voting channel is null"() {
    when:
    service.markAsUsed(6, 1, null)

    then:
    thrown(IllegalArgumentException)
  }

  def "markAsBlocked should update the card's voting right status to BLOCKED"() {
    def votingCard = createVotingCard(1, 10)

    given:
    operationRepository.findById(_) >> Optional.of(votingCard.operation)
    votingCardRepository.findByOperation_IdAndVoterIndex(6, 1) >> Optional.of(votingCard)
    votingCardRepository.save(_) >> votingCard

    when:
    service.markAsBlocked(6, 1)
    def vc = service.findByOperationIdAndVoterIndex(6, 1).get()

    then:
    vc.id == 10
    vc.votingRightStatus == VotingRightStatus.BLOCKED
  }

  def "markAsBlocked should throw a VotingRightStatusException when the card's voting right status doesn't allow a status update"() {
    def votingCard = createVotingCard(1, 10)
    votingCard.votingRightStatus = votingRightStatus

    given:
    votingCardRepository.findByOperation_IdAndVoterIndex(6, 1) >> Optional.of(votingCard)

    when:
    service.markAsBlocked(6, 1)

    then:
    thrown(IllegalVotingRightStatusTransitionException)

    where:
    votingRightStatus << [
            VotingRightStatus.USED,
            VotingRightStatus.BLOCKED,
            VotingRightStatus.LOCKED
    ]
  }

  def "findByOperationIdAndIdentificationCodeHash should return a list of matching voting cards"() {
    def votingCardStub = createVotingCard(0, 0)

    given:
    operationRepository.findById(_) >> Optional.of(votingCardStub.operation)
    votingCardRepository.findByOperation_IdAndIdentificationCodeHash(6, "ID_HASH_1") >>
            [votingCardStub].stream()

    when:
    def votingCards = service.findByOperationIdAndIdentificationCodeHash(6, "ID_HASH_1")

    then:
    votingCards[0].id == votingCardStub.id
    votingCards[0].operationId == votingCardStub.operation.id
    votingCards[0].voterIndex == votingCardStub.voterIndex
    votingCards[0].identificationCodeHash == votingCardStub.identificationCodeHash
    votingCards[0].localPersonId == votingCardStub.localPersonId
    votingCards[0].voterBirthDate.voterBirthDay == votingCardStub.voterBirthDay
    votingCards[0].voterBirthDate.voterBirthMonth == votingCardStub.voterBirthMonth
    votingCards[0].voterBirthDate.voterBirthYear == votingCardStub.voterBirthYear
    votingCards[0].votingDatetime != null
    votingCards[0].votingRightStatus == votingCardStub.votingRightStatus
    votingCards[0].countingCircle.businessId == votingCardStub.countingCircle.businessId
    votingCards[0].domainOfInfluences.size() == votingCardStub.domainOfInfluences.size()
  }

  def "findByOperationIdAndIdentificationCodeHash should throw an exception if no Operation corresponds to the given parameters"() {
    def identificationCodeHash = "0000"

    given:
    operationRepository.findById(_) >> Optional.empty()
    votingCardRepository.findByOperation_IdAndIdentificationCodeHash(6, identificationCodeHash) >>
            [].stream()

    when:
    service.findByOperationIdAndIdentificationCodeHash(6, identificationCodeHash)

    then:
    thrown(EntityNotFoundException)
  }

  def "findByOperationIdAndLocalPersonId should return a list of matching voting cards"() {
    def votingCardStub = createVotingCard(1, 1)

    given:
    operationRepository.findById(_) >> Optional.of(votingCardStub.operation)
    votingCardRepository.findByOperation_IdAndLocalPersonId(6, "L_PERSON_ID_1") >>
            [votingCardStub].stream()

    when:
    def votingCards = service.findByOperationIdAndLocalPersonId(6, "L_PERSON_ID_1")

    then:
    votingCards[0].id == votingCardStub.id
    votingCards[0].operationId == votingCardStub.operation.id
    votingCards[0].voterIndex == votingCardStub.voterIndex
    votingCards[0].identificationCodeHash == votingCardStub.identificationCodeHash
    votingCards[0].localPersonId == votingCardStub.localPersonId
    votingCards[0].voterBirthDate.voterBirthDay == votingCardStub.voterBirthDay
    votingCards[0].voterBirthDate.voterBirthMonth == votingCardStub.voterBirthMonth
    votingCards[0].voterBirthDate.voterBirthYear == votingCardStub.voterBirthYear
    votingCards[0].votingDatetime != null
    votingCards[0].votingRightStatus == votingCardStub.votingRightStatus
    votingCards[0].countingCircle.businessId == votingCardStub.countingCircle.businessId
    votingCards[0].domainOfInfluences.size() == votingCardStub.domainOfInfluences.size()
  }

  def "findByOperationIdAndLocalPersonId should throw an exception if no Operation corresponds to the given parameters"() {
    def localPersonId = "0000"

    given:
    operationRepository.findById(_) >> Optional.empty()
    votingCardRepository.findByOperation_IdAndLocalPersonId(6, localPersonId) >> [].stream()

    when:
    service.findByOperationIdAndLocalPersonId(6, localPersonId)

    then:
    thrown(EntityNotFoundException)
  }

  def createVotingCard(Long voterIndex, Long id, VotingRightStatus status = VotingRightStatus.AVAILABLE) {
    def votingCard = new VotingCard()

    votingCard.id = id
    votingCard.operation = createOperation(6)
    votingCard.voterIndex = voterIndex
    votingCard.identificationCodeHash = "ID_HASH_1"
    votingCard.localPersonId == "L_PERSON_ID_1"
    votingCard.voterBirthYear = 1970
    votingCard.voterBirthMonth = 4
    votingCard.voterBirthDay = 5
    votingCard.votingDatetime = LocalDateTime.now()
    votingCard.votingRightStatus = status
    votingCard.votingChannel = VotingChannel.E_VOTING
    votingCard.countingCircle = new CountingCircle()
    votingCard.countingCircle.name = "Carouge"
    votingCard.domainOfInfluences = [new DomainOfInfluence(), new DomainOfInfluence()]
    votingCard.domainOfInfluences[0].name = "Carouge"
    votingCard.domainOfInfluences[1].name = "Genève"

    return votingCard
  }

  def createOperation(Long id) {
    def operation = new Operation()
    operation.id = id

    return operation
  }

}