/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.service.notification;

import ch.ge.ve.chvote.pact.b2b.client.PactB2BClient;
import ch.ge.ve.chvote.pact.b2b.client.model.Voter;
import ch.ge.ve.vrum.repository.operation.OperationRepository;
import ch.ge.ve.vrum.repository.operation.entity.Operation;
import ch.ge.ve.vrum.repository.votingcard.CountingCircleRepository;
import ch.ge.ve.vrum.repository.votingcard.DomainOfInfluenceRepository;
import ch.ge.ve.vrum.repository.votingcard.VotingCardRepository;
import ch.ge.ve.vrum.repository.votingcard.entity.CountingCircle;
import ch.ge.ve.vrum.repository.votingcard.entity.DomainOfInfluence;
import ch.ge.ve.vrum.repository.votingcard.entity.VotingCard;
import ch.ge.ve.vrum.repository.votingcard.entity.VotingRightStatus;
import ch.ge.ve.vrum.service.mapper.BeanMapper;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Initializer class that is responsible of the initialization of an operation and all of its voting cards.
 */
@Service
public class OperationInitializer {
  private final PactB2BClient               pactB2BRestClient;
  private final OperationRepository         operationRepository;
  private final VotingCardRepository        votingCardRepository;
  private final DomainOfInfluenceRepository domainOfInfluenceRepository;
  private final CountingCircleRepository    countingCircleRepository;

  /**
   * Create a new operation initializer.
   *
   * @param pactB2BClient               a client to import the operation and voting cards metadata from the PACT.
   * @param operationRepository         an operation repository to save the imported operation metadata.
   * @param votingCardRepository        the voting card repository the save the imported voting cards metadata.
   * @param domainOfInfluenceRepository the domain of influence repository to save the imported voting cards domain of
   *                                    influences metadata.
   * @param countingCircleRepository    the counting circle repository to save the imported voting cards counting
   *                                    circles metadata.
   */
  @Autowired
  public OperationInitializer(PactB2BClient pactB2BClient,
                              OperationRepository operationRepository,
                              VotingCardRepository votingCardRepository,
                              DomainOfInfluenceRepository domainOfInfluenceRepository,
                              CountingCircleRepository countingCircleRepository) {
    this.pactB2BRestClient = pactB2BClient;
    this.operationRepository = operationRepository;
    this.votingCardRepository = votingCardRepository;
    this.domainOfInfluenceRepository = domainOfInfluenceRepository;
    this.countingCircleRepository = countingCircleRepository;
  }

  /**
   * Initialize the operation identified by the given protocol identifier. This method will request the operation and
   * all of its voting cards data and store them. This method will block the execution until importing task is
   * successful or an error occurs.
   *
   * @param protocolId the protocol identifier.
   */
  @Transactional
  public void initializeOperation(String protocolId) {
    Operation operation = operationRepository.save(
        BeanMapper.map(pactB2BRestClient.getOperationConfiguration(protocolId), protocolId)
    );

    pactB2BRestClient.getVoters(operation.getProtocolId())
                     .stream()
                     .map(voter -> this.createVotingCard(voter, operation))
                     .forEach(votingCardRepository::save);
  }

  private VotingCard createVotingCard(Voter voter, Operation operation) {
    VotingCard votingCard = BeanMapper.map(voter);

    votingCard.setOperation(operation);

    votingCard.setDomainOfInfluences(
        voter.getDomainOfInfluenceIds()
             .stream()
             .map(this::getOrCreateDomainOfInfluence)
             .collect(Collectors.toList())
    );

    votingCard.setCountingCircle(getOrCreateCountingCircles(voter.getCountingCircle()));

    votingCard.setVotingRightStatus(VotingRightStatus.AVAILABLE);

    return votingCard;
  }

  private DomainOfInfluence getOrCreateDomainOfInfluence(ch.ge.ve.protocol.model.DomainOfInfluence domainOfInfluence) {
    return domainOfInfluenceRepository
        .findByBusinessId(domainOfInfluence.getIdentifier())
        .orElseGet(() -> domainOfInfluenceRepository.save(BeanMapper.map(domainOfInfluence)));
  }

  private CountingCircle getOrCreateCountingCircles(ch.ge.ve.protocol.model.CountingCircle countingCircle) {
    return countingCircleRepository
        .findByBusinessId(countingCircle.getBusinessId())
        .orElseGet(() -> countingCircleRepository.save(BeanMapper.map(countingCircle)));
  }

}
