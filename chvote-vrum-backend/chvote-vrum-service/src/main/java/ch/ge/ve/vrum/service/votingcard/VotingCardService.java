/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.service.votingcard;

import ch.ge.ve.vrum.repository.operation.OperationRepository;
import ch.ge.ve.vrum.repository.operation.entity.Operation;
import ch.ge.ve.vrum.repository.votingcard.VotingCardRepository;
import ch.ge.ve.vrum.repository.votingcard.entity.VotingCard;
import ch.ge.ve.vrum.repository.votingcard.entity.VotingChannel;
import ch.ge.ve.vrum.repository.votingcard.entity.VotingRightStatus;
import ch.ge.ve.vrum.service.exception.EntityNotFoundException;
import ch.ge.ve.vrum.service.exception.IllegalVotingRightStatusTransitionException;
import ch.ge.ve.vrum.service.mapper.BeanMapper;
import ch.ge.ve.vrum.service.votingcard.model.VotingCardDto;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service for managing voting cards.
 */
@Service
public class VotingCardService {

  private final VotingCardRepository votingCardRepository;
  private final OperationRepository  operationRepository;

  /**
   * Create a new voting card service.
   *
   * @param votingCardRepository the {@link VotingCard} repository.
   * @param operationRepository  the {@link ch.ge.ve.vrum.repository.operation.entity.Operation} repository
   */
  @Autowired
  public VotingCardService(VotingCardRepository votingCardRepository,
                           OperationRepository operationRepository) {
    this.votingCardRepository = votingCardRepository;
    this.operationRepository = operationRepository;
  }

  /**
   * Retrieves the voting card matching the given operation id and voter index.
   *
   * @param operationId the operation id.
   * @param voterIndex  the voter index.
   *
   * @return the matching voting card.
   *
   * @throws EntityNotFoundException if the operation does not exists
   */
  @Transactional(readOnly = true)
  public Optional<VotingCardDto> findByOperationIdAndVoterIndex(Long operationId, Long voterIndex) {
    Operation operation = findOperation(operationId);
    Optional<VotingCard> votingCard =
        votingCardRepository.findByOperation_IdAndVoterIndex(operation.getId(), voterIndex);

    return votingCard.map(BeanMapper::map);
  }

  /**
   * Registers the use of a {@link VotingCard}'s voting right.
   *
   * @param operationId   the operation id
   * @param voterIndex    the voter index
   * @param votingChannel the voting channel used to cast the vote
   *
   * @throws EntityNotFoundException                     if no voting card matching the given parameters could be found
   * @throws IllegalVotingRightStatusTransitionException if the card's status doesn't allow for a status update
   */
  @Transactional
  public void markAsUsed(Long operationId, Long voterIndex, VotingChannel votingChannel) {
    if (votingChannel == null) {
      throw new IllegalArgumentException("The voting channel cannot be null.");
    }

    VotingCard votingCard = findVotingCard(operationId, voterIndex);

    if (votingCard.getVotingRightStatus() != VotingRightStatus.AVAILABLE) {
      throw new IllegalVotingRightStatusTransitionException(BeanMapper.map(votingCard));
    }

    votingCard.setVotingRightStatus(VotingRightStatus.USED);
    votingCard.setVotingChannel(votingChannel);
    votingCard.setVotingDatetime(LocalDateTime.now());

    this.votingCardRepository.save(votingCard);
  }

  /**
   * Blocks a {@link VotingCard}.
   *
   * @param operationId the operation id
   * @param voterIndex  the voter index
   *
   * @throws EntityNotFoundException                     if no voting card matching the given parameters could be found
   * @throws IllegalVotingRightStatusTransitionException if the card's status doesn't allow for a status update
   */
  @Transactional
  public void markAsBlocked(Long operationId, Long voterIndex) {
    VotingCard votingCard = findVotingCard(operationId, voterIndex);

    if (votingCard.getVotingRightStatus() != VotingRightStatus.AVAILABLE) {
      throw new IllegalVotingRightStatusTransitionException(BeanMapper.map(votingCard));
    }

    votingCard.setVotingRightStatus(VotingRightStatus.BLOCKED);

    this.votingCardRepository.save(votingCard);
  }

  /**
   * Retrieves the voting card matching the given operation id and identification code hash.
   *
   * @param operationId            the operation id.
   * @param identificationCodeHash the hash of the identification code.
   *
   * @return the matching voting card.
   *
   * @throws EntityNotFoundException if the operation does not exist
   */
  @Transactional(readOnly = true)
  public List<VotingCardDto> findByOperationIdAndIdentificationCodeHash(Long operationId,
                                                                        String identificationCodeHash) {
    Operation operation = findOperation(operationId);

    return votingCardRepository
        .findByOperation_IdAndIdentificationCodeHash(operation.getId(), identificationCodeHash)
        .map(BeanMapper::map)
        .collect(Collectors.toList());
  }

  /**
   * Retrieves the voting card matching the given operation id and local person id.
   *
   * @param operationId   the operation id.
   * @param localPersonId the local person id.
   *
   * @return the matching voting card.
   *
   * @throws EntityNotFoundException if the operation does not exist
   */
  @Transactional(readOnly = true)
  public List<VotingCardDto> findByOperationIdAndLocalPersonId(Long operationId, String localPersonId) {
    Operation operation = findOperation(operationId);

    return votingCardRepository
        .findByOperation_IdAndLocalPersonId(operation.getId(), localPersonId)
        .map(BeanMapper::map)
        .collect(Collectors.toList());
  }

  private VotingCard findVotingCard(Long operationId, Long voterIndex) {
    return votingCardRepository.findByOperation_IdAndVoterIndex(operationId, voterIndex)
                               .orElseThrow(() -> new EntityNotFoundException(String.format(
                                   "Cannot find any voting card with voter index [%s] for operation [%s]", voterIndex,
                                   operationId)));
  }

  private Operation findOperation(Long operationId) {
    return operationRepository.findById(operationId)
                              .orElseThrow(() -> new EntityNotFoundException(
                                  String.format("Cannot find any operation with operation id [%s]", operationId)));
  }

}
