/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.service.votingcard.model;

import java.util.Objects;

/**
 * DTO class that represents a voter's birth date.
 */
public class VoterBirthDateDto {

  private final Integer voterBirthYear;
  private final Integer voterBirthMonth;
  private final Integer voterBirthDay;

  /**
   * Create a new voter birth date DTO.
   *
   * @param voterBirthYear  the voter birth year (mandatory).
   * @param voterBirthMonth the voter birth month (optional).
   * @param voterBirthDay   the voter birth day (optional).
   */
  public VoterBirthDateDto(Integer voterBirthYear, Integer voterBirthMonth, Integer voterBirthDay) {
    Objects.requireNonNull(voterBirthYear);

    this.voterBirthYear = voterBirthYear;
    this.voterBirthMonth = voterBirthMonth;
    this.voterBirthDay = voterBirthDay;
  }

  public Integer getVoterBirthYear() {
    return voterBirthYear;
  }

  public Integer getVoterBirthMonth() {
    return voterBirthMonth;
  }

  public Integer getVoterBirthDay() {
    return voterBirthDay;
  }

}
