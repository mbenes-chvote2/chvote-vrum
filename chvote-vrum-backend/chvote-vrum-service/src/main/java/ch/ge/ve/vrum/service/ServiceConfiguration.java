/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.service;

import ch.ge.ve.chvote.pact.b2b.client.PactB2BClient;
import ch.ge.ve.chvote.pact.b2b.client.PactB2BRestClient;
import ch.ge.ve.chvote.pact.b2b.client.PactB2BRestClientConfiguration;
import ch.ge.ve.jacksonserializer.BigIntegerAsBase64Deserializer;
import ch.ge.ve.jacksonserializer.BigIntegerAsBase64Serializer;
import ch.ge.ve.jacksonserializer.JSDates;
import ch.ge.ve.vrum.repository.RepositoryConfiguration;
import ch.ge.ve.vrum.service.notification.NotificationListenerConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Configuration class for the <code>chvote-vrum-service</code> module.
 */
@Configuration
@ComponentScan("ch.ge.ve.vrum.service")
@Import({RepositoryConfiguration.class, NotificationListenerConfiguration.class})
public class ServiceConfiguration {

  @Bean
  public PactB2BClient pactB2BClient(PactB2BRestClientConfiguration pactConfiguration,
                                     RestTemplateBuilder restTemplateBuilder) {
    return new PactB2BRestClient(pactConfiguration, restTemplateBuilder);
  }

  @Bean
  public PactB2BRestClientConfiguration pactB2BRestClientConfiguration(
      @Value("${pactb2b.username}") String username,
      @Value("${pactb2b.password}") String password,
      @Value("${pactb2b.rootURI}") String rootURI) {
    return new PactB2BRestClientConfiguration() {
      @Override
      public String getUsername() {
        return username;
      }

      @Override
      public String getPassword() {
        return password;
      }

      @Override
      public String getRootURI() {
        return rootURI;
      }
    };
  }

  @Bean
  public Jackson2ObjectMapperBuilderCustomizer jackson2ObjectMapperBuilderCustomizer() {
    return jacksonObjectMapperBuilder -> {
      jacksonObjectMapperBuilder.serializers(JSDates.SERIALIZER);
      jacksonObjectMapperBuilder.deserializers(JSDates.DESERIALIZER);

      jacksonObjectMapperBuilder.serializers(new BigIntegerAsBase64Serializer());
      jacksonObjectMapperBuilder.deserializers(new BigIntegerAsBase64Deserializer());
    };
  }
}
