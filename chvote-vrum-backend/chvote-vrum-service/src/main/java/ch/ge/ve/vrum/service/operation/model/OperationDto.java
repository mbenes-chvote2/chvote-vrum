/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.service.operation.model;

import java.time.LocalDateTime;

/**
 * DTO class for the {@link ch.ge.ve.vrum.repository.operation.entity.Operation} entity.
 */
public final class OperationDto {

  private final Long          id;
  private final LocalDateTime operationDate;
  private final String        shortLabel;
  private final String        longLabel;
  private final boolean       test;

  /**
   * Creates a new operation DTO.
   *
   * @param id            the id value.
   * @param operationDate the operation date value.
   * @param shortLabel    the short label value.
   * @param longLabel     the long label value.
   * @param test          whether this is a test operation.
   */
  public OperationDto(Long id,
                      LocalDateTime operationDate,
                      String shortLabel,
                      String longLabel,
                      boolean test) {
    this.id = id;
    this.operationDate = operationDate;
    this.shortLabel = shortLabel;
    this.longLabel = longLabel;
    this.test = test;
  }

  public Long getId() {
    return id;
  }

  public LocalDateTime getOperationDate() {
    return operationDate;
  }

  public String getShortLabel() {
    return shortLabel;
  }

  public String getLongLabel() {
    return longLabel;
  }

  public boolean isTest() {
    return test;
  }
}
