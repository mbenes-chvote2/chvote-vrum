/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.service.votingcard.model;

import ch.ge.ve.vrum.repository.votingcard.entity.VotingChannel;
import ch.ge.ve.vrum.repository.votingcard.entity.VotingRightStatus;
import java.time.LocalDateTime;
import java.util.List;

/**
 * DTO class for the {@link ch.ge.ve.vrum.repository.votingcard.entity.VotingCard} entity.
 */
public class VotingCardDto {

  private final Long                       id;
  private final Long                       operationId;
  private final Long                       voterIndex;
  private final String                     identificationCodeHash;
  private final String                     localPersonId;
  private final VoterBirthDateDto          voterBirthDate;
  private final LocalDateTime              votingDatetime;
  private final VotingRightStatus          votingRightStatus;
  private final VotingChannel              votingChannel;
  private final CountingCircleDto          countingCircle;
  private final List<DomainOfInfluenceDto> domainOfInfluences;

  /**
   * Create a new voting card DTO.
   *
   * @param id                     the voting card technical id.
   * @param operationId            the operation technical id.
   * @param voterIndex             the voter index for this operation.
   * @param identificationCodeHash a hash of the identification code on the voting card.
   * @param localPersonId          the local person id.
   * @param voterBirthDate         the voter's birth date
   * @param votingDatetime         the date when the vote was casted.
   * @param votingRightStatus      the current voting right status.
   * @param votingChannel          the channel through which the vote was casted.
   * @param countingCircle         the counting circle.
   * @param domainOfInfluences     all the domains of influence of this voting card.
   */
  public VotingCardDto(Long id,
                       Long operationId,
                       Long voterIndex,
                       String identificationCodeHash,
                       String localPersonId,
                       VoterBirthDateDto voterBirthDate,
                       LocalDateTime votingDatetime,
                       VotingRightStatus votingRightStatus,
                       VotingChannel votingChannel,
                       CountingCircleDto countingCircle,
                       List<DomainOfInfluenceDto> domainOfInfluences) {
    this.id = id;
    this.operationId = operationId;
    this.voterIndex = voterIndex;
    this.identificationCodeHash = identificationCodeHash;
    this.localPersonId = localPersonId;
    this.voterBirthDate = voterBirthDate;
    this.votingDatetime = votingDatetime;
    this.votingRightStatus = votingRightStatus;
    this.votingChannel = votingChannel;
    this.countingCircle = countingCircle;
    this.domainOfInfluences = domainOfInfluences;
  }

  public Long getId() {
    return id;
  }

  public Long getOperationId() {
    return operationId;
  }

  public Long getVoterIndex() {
    return voterIndex;
  }

  public String getIdentificationCodeHash() {
    return identificationCodeHash;
  }

  public String getLocalPersonId() {
    return localPersonId;
  }

  public VoterBirthDateDto getVoterBirthDate() {
    return voterBirthDate;
  }

  public LocalDateTime getVotingDatetime() {
    return votingDatetime;
  }

  public VotingRightStatus getVotingRightStatus() {
    return votingRightStatus;
  }

  public VotingChannel getVotingChannel() {
    return votingChannel;
  }

  public CountingCircleDto getCountingCircle() {
    return countingCircle;
  }

  public List<DomainOfInfluenceDto> getDomainOfInfluences() {
    return domainOfInfluences;
  }
}
