/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.service.mapper;

import ch.ge.ve.chvote.pact.b2b.client.model.OperationBaseConfiguration;
import ch.ge.ve.chvote.pact.b2b.client.model.OperationType;
import ch.ge.ve.chvote.pact.b2b.client.model.Voter;
import ch.ge.ve.vrum.repository.operation.entity.Canton;
import ch.ge.ve.vrum.repository.operation.entity.Operation;
import ch.ge.ve.vrum.repository.votingcard.entity.CountingCircle;
import ch.ge.ve.vrum.repository.votingcard.entity.DomainOfInfluence;
import ch.ge.ve.vrum.repository.votingcard.entity.VotingCard;
import ch.ge.ve.vrum.service.operation.model.OperationDto;
import ch.ge.ve.vrum.service.votingcard.model.CountingCircleDto;
import ch.ge.ve.vrum.service.votingcard.model.DomainOfInfluenceDto;
import ch.ge.ve.vrum.service.votingcard.model.VoterBirthDateDto;
import ch.ge.ve.vrum.service.votingcard.model.VotingCardDto;
import java.util.stream.Collectors;

/**
 * Utility class for mapping entities into Data Transfer Objects (DTO).
 */
public class BeanMapper {

  private BeanMapper() {
    throw new IllegalStateException("Utility class");
  }

  /**
   * Map an {@link Operation} entity to its corresponding {@link OperationDto} model.
   *
   * @param operation the operation entity.
   *
   * @return the corresponding operation model.
   */
  public static OperationDto map(Operation operation) {
    return new OperationDto(operation.getId(),
                            operation.getOperationDate(),
                            operation.getShortLabel(),
                            operation.getLongLabel(),
                            operation.isTest());
  }

  /**
   * Map a {@link VotingCard} entity to its corresponding {@link VotingCardDto} model.
   *
   * @param votingCard the voting card entity.
   *
   * @return the corresponding voting card model.
   */
  public static VotingCardDto map(VotingCard votingCard) {
    return new VotingCardDto(
        votingCard.getId(),
        votingCard.getOperation().getId(),
        votingCard.getVoterIndex(),
        votingCard.getIdentificationCodeHash(),
        votingCard.getLocalPersonId(),
        new VoterBirthDateDto(votingCard.getVoterBirthYear(),
                              votingCard.getVoterBirthMonth(),
                              votingCard.getVoterBirthDay()),
        votingCard.getVotingDatetime(),
        votingCard.getVotingRightStatus(),
        votingCard.getVotingChannel(),
        map(votingCard.getCountingCircle()),
        votingCard.getDomainOfInfluences()
                  .stream()
                  .map(BeanMapper::map)
                  .collect(Collectors.toList())
    );
  }

  /**
   * Map a {@link Voter} model to its corresponding {@link VotingCard} entity.
   *
   * @param voter the voter model.
   *
   * @return the corresponding voting card entity.
   */
  public static VotingCard map(Voter voter) {
    VotingCard votingCard = new VotingCard();

    votingCard.setVoterIndex(Long.valueOf(voter.getVoterIndex()));
    votingCard.setIdentificationCodeHash(voter.getIdentificationCodeHash());
    votingCard.setLocalPersonId(voter.getLocalPersonId());
    votingCard.setVoterBirthYear(voter.getVoterBirthYear());
    votingCard.setVoterBirthMonth(voter.getVoterBirthMonth());
    votingCard.setVoterBirthDay(voter.getVoterBirthDay());

    return votingCard;
  }

  /**
   * Map a {@link ch.ge.ve.protocol.model.DomainOfInfluence} model to its corresponding {@link DomainOfInfluence}
   * entity.
   *
   * @param input the domain of influence model.
   *
   * @return the corresponding domain of influence entity.
   */
  public static DomainOfInfluence map(ch.ge.ve.protocol.model.DomainOfInfluence input) {
    DomainOfInfluence output = new DomainOfInfluence();
    output.setBusinessId(input.getIdentifier());
    // TODO Add domain of influence name to the protocol model.
    output.setName(input.getIdentifier());
    return output;
  }

  /**
   * Map a {@link ch.ge.ve.protocol.model.CountingCircle} model to its corresponding {@link CountingCircle} entity.
   *
   * @param input the counting circle model.
   *
   * @return the corresponding counting circle entity.
   */
  public static CountingCircle map(ch.ge.ve.protocol.model.CountingCircle input) {
    CountingCircle output = new CountingCircle();
    output.setBusinessId(input.getBusinessId());
    output.setName(input.getName());
    return output;
  }

  /**
   * Map a {@link CountingCircle} to its corresponding {@link CountingCircleDto} model.
   *
   * @param countingCircle the counting circle entity
   *
   * @return the corresponding counting circle model
   */
  public static CountingCircleDto map(CountingCircle countingCircle) {
    return new CountingCircleDto(countingCircle.getBusinessId(), countingCircle.getName());
  }

  /**
   * Map a {@link DomainOfInfluence} to its corresponding {@link DomainOfInfluenceDto} model.
   *
   * @param domainOfInfluence the domain of influence entity
   *
   * @return the corresponding domain of influence model
   */
  public static DomainOfInfluenceDto map(DomainOfInfluence domainOfInfluence) {
    return new DomainOfInfluenceDto(domainOfInfluence.getBusinessId(), domainOfInfluence.getName());
  }

  /**
   * Map a {@link OperationBaseConfiguration} model to a {@link Operation} entity.
   *
   * @param operationBaseConfiguration the operation configuration model to be mapped.
   * @param protocolId                 the protocol id of the mapped operation entity.
   *
   * @return The mapped {@link Operation} entity.
   */
  public static Operation map(OperationBaseConfiguration operationBaseConfiguration, String protocolId) {
    Operation operation = new Operation();

    operation.setProtocolId(protocolId);
    operation.setCanton(Canton.valueOf(operationBaseConfiguration.getCanton().name()));
    // TODO check whether this should be simulation and all OperationType.TEST should be filtered out in VRUM
    operation.setTest(operationBaseConfiguration.getType() == OperationType.TEST);
    operation.setOperationDate(operationBaseConfiguration.getOperationDate());
    operation.setLongLabel(operationBaseConfiguration.getLongLabel());
    operation.setShortLabel(operationBaseConfiguration.getShortLabel());

    return operation;
  }
}
