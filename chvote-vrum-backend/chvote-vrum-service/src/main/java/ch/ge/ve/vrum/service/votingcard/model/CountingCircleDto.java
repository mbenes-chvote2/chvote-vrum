/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.service.votingcard.model;

/**
 * DTO class for the {@link ch.ge.ve.vrum.repository.votingcard.entity.CountingCircle} entity.
 */
public class CountingCircleDto {

  private final String businessId;
  private final String name;

  /**
   * Create a new counting circle DTO.
   *
   * @param businessId the counting circle business ID.
   * @param name       the counting circle name.
   */
  public CountingCircleDto(String businessId, String name) {
    this.businessId = businessId;
    this.name = name;
  }

  public String getBusinessId() {
    return businessId;
  }

  public String getName() {
    return name;
  }

}
