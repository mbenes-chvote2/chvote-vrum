/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.rest.api.controller

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get

import ch.ge.ve.vrum.rest.api.APIEndpointTest
import ch.ge.ve.vrum.service.operation.OperationService
import ch.ge.ve.vrum.service.operation.model.OperationDto
import groovy.json.JsonSlurper
import java.time.LocalDateTime
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

@APIEndpointTest
class OperationControllerTest extends Specification {

  @Autowired
  OperationService service

  @Autowired
  MockMvc mvc

  JsonSlurper jsonSlurper = new JsonSlurper()

  def static operationsStub = [
          createOperation(1),
          createOperation(2),
          createOperation(3),
          createOperation(4)
  ]

  @WithMockUser(username = "test", roles = ["SELECT_OPERATION"])
  def "findAll should return all the operations"() {
    given:
    service.findAll() >> operationsStub

    when:
    def response = mvc.perform(get("/operations")).andReturn().response
    def content = jsonSlurper.parseText(response.contentAsString)

    then:
    response.status == HttpStatus.OK.value()
    content.size == 4
  }

  @WithMockUser(username = "test", roles = ["ACCESS_CARD_PROCESSING"])
  def "findAll should respond with a 403 status code when the user does not have the SELECT_OPERATION role"() {
    when:
    def response = mvc.perform(get("/operations")).andReturn().response

    then:
    response.status == HttpStatus.FORBIDDEN.value()
  }

  @WithMockUser(username = "test", roles = ["SELECT_OPERATION"])
  def "findByTest should return exclusively test operations OR real operations"() {
    given:
    service.findByTest(test) >> expected

    when:
    def response = mvc.perform(get("/operations?test=${test}")).andReturn().response
    def content = jsonSlurper.parseText(response.contentAsString)

    then:
    response.status == HttpStatus.OK.value()
    content.size == operationsCount

    where:
    test  | expected               | operationsCount
    true  | [operationsStub[3]]    | 1
    false | operationsStub.take(3) | 3
  }

  @WithMockUser(username = "test", roles = ["ACCESS_CARD_PROCESSING"])
  def "findByTest should return an AccessDeniedException when the user does not have the SELECT_OPERATION role"() {
    when:
    def response = mvc.perform(get("/operations?test=true")).andReturn().response

    then:
    response.status == HttpStatus.FORBIDDEN.value()
  }

  @WithMockUser(username = "test", roles = ["SELECT_OPERATION"])
  def "findOne should return only one operation"() {
    given:
    service.findById(1) >> operationsStub[0]

    when:
    def response = mvc.perform(get("/operations/1")).andReturn().response
    def content = jsonSlurper.parseText(response.contentAsString)

    then:
    response.status == HttpStatus.OK.value()
    content.id == operationsStub[0].id
  }

  @WithMockUser(username = "test", roles = ["ACCESS_CARD_PROCESSING"])
  def "findOne should return an AccessDeniedException when the user does not have the SELECT_OPERATION role"() {
    when:
    def response = mvc.perform(get("/operations/1")).andReturn().response

    then:
    response.status == HttpStatus.FORBIDDEN.value()
  }

  def static createOperation(Long id, boolean test = false) {
    return new OperationDto(id, LocalDateTime.now(), "Operation " + id, "Long Label Operation " + id, test)
  }

}
