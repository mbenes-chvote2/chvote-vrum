/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.rest.api.controller.version

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get

import ch.ge.ve.vrum.rest.api.APIEndpointTest
import groovy.json.JsonSlurper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

@APIEndpointTest
class VersionControllerTest extends Specification {

  @Autowired
  MockMvc mvc

  JsonSlurper jsonSlurper = new JsonSlurper()

  @WithMockUser(username = "test", roles = "USER")
  def "getCurrentVersion should return the current application version"() {
    when:
    def response = mvc.perform(get("/version")).andReturn().response
    def content = jsonSlurper.parseText(response.getContentAsString())

    then: "the body corresponds to the values in the application.yml"
    response.status == 200
    content.buildNumber == "test"
    content.buildTimestamp == "2018-11-09T16:27:00"
  }

}
