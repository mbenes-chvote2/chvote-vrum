/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.rest.api

import ch.ge.ve.vrum.service.operation.OperationService
import ch.ge.ve.vrum.service.votingcard.VotingCardService
import org.springframework.context.annotation.Bean
import spock.mock.DetachedMockFactory

/**
 * Declare mock beans for unit tests.
 */
class APIEndpointTestConfiguration {

  def detachedMockFactory = new DetachedMockFactory()

  @Bean
  OperationService operationService() {
    detachedMockFactory.Mock(OperationService)
  }

  @Bean
  VotingCardService votingCardService() {
    detachedMockFactory.Mock(VotingCardService)
  }

}
