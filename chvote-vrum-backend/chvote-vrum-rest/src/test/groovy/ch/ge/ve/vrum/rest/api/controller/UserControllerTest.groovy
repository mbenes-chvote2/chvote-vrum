/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.rest.api.controller

import ch.ge.ve.vrum.rest.api.APIEndpointTest
import ch.ge.ve.vrum.rest.shared.model.ConnectedUser
import groovy.json.JsonSlurper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import spock.lang.Specification

@APIEndpointTest
class UserControllerTest extends Specification {

  @Autowired
  MockMvc mvc

  JsonSlurper jsonSlurper = new JsonSlurper()

  @WithMockUser(username = "technical", roles = ["USER"])
  def "getConnectedUser should return the currently connected user"() {
    given: "technical user is logged in"
    ConnectedUser.technical()

    when:
    def response = mvc.perform(MockMvcRequestBuilders.get("/user/me")).andReturn().response
    def content = jsonSlurper.parseText(response.contentAsString)

    then:
    response.status == HttpStatus.OK.value()
    content.login == "technical"
    content.roles == ["ROLE_TECHNICAL"]
    content.realm == null
    content.managementEntity == "technical"
  }

}
