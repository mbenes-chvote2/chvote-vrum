/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.rest.api.controller;

import ch.ge.ve.vrum.repository.votingcard.entity.VotingChannel;
import ch.ge.ve.vrum.repository.votingcard.entity.VotingRightStatus;
import ch.ge.ve.vrum.service.exception.EntityNotFoundException;
import ch.ge.ve.vrum.service.exception.IllegalVotingRightStatusTransitionException;
import ch.ge.ve.vrum.service.votingcard.VotingCardService;
import ch.ge.ve.vrum.service.votingcard.model.VotingCardDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * A {@link VotingCardDto} REST controller.
 */
@RestController
@RequestMapping("/operations/{operationId}/voting-cards")
@Api(value = "Voting cards", tags = "Voting cards")
public class VotingCardController {

  private static final Logger logger = LoggerFactory.getLogger(VotingCardController.class);

  private final VotingCardService votingCardService;

  /**
   * Create a new voting card controller.
   *
   * @param votingCardService the {@link VotingCardService}.
   */
  @Autowired
  public VotingCardController(VotingCardService votingCardService) {
    this.votingCardService = votingCardService;
  }

  /**
   * Retrieve a matching {@link VotingCardDto}.
   *
   * @param operationId the operation id.
   * @param voterIndex  the voter index.
   *
   * @return a matching voting card.
   *
   * @throws EntityNotFoundException if no voting card matching the given parameters could be found.
   */
  @GetMapping(value = "{voterIndex}", produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(
      value = "Retrieve a voting card",
      notes = "Retrieve a voting card by its associated voter index." +
              "###Required user roles\n\n" +
              "* ROLE_ACCESS_CARD_PROCESSING" +
              "* ROLE_CARD_PROCESSING_SELECT_VOTING_CARD")
  @ApiResponses(value = {
      @ApiResponse(code = 403, message = "The current user is not authorized to access this resource"),
      @ApiResponse(code = 404, message = "Voting card not found")
  })
  @PreAuthorize("hasRole('ACCESS_CARD_PROCESSING') and hasRole('CARD_PROCESSING_SELECT_VOTING_CARD')")
  public VotingCardDto findByOperationIdAndVoterIndex(
      @ApiParam(value = "The operation id") @PathVariable("operationId") Long operationId,
      @ApiParam(value = "Voter index") @PathVariable("voterIndex") Long voterIndex) {
    return votingCardService
        .findByOperationIdAndVoterIndex(operationId, voterIndex)
        .orElseThrow(() -> new EntityNotFoundException(String.format(
            "Cannot find any voting card with voter index [%s] for operation [%s]", voterIndex, operationId)));
  }

  /**
   * Updates the {@link ch.ge.ve.vrum.repository.votingcard.entity.VotingCard#votingRightStatus}.
   *
   * @param operationId   the operation id.
   * @param voterIndex    the voter index.
   * @param votingChannel the voting channel used to cast the vote (optional).
   *
   * @throws EntityNotFoundException                     if there were no entities matching the given parameters.
   * @throws IllegalVotingRightStatusTransitionException if the voting card isn't in a state allowing the vote.
   */
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @PatchMapping(value = "{voterIndex}", produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(
      value = "Update a voting card's voting right status",
      notes = "Since it is not possible to update the status of a voting card" +
              " if it's used, blocked, or locked," +
              " the API will return a 409 error along with the current card's status" +
              " to let the client know why the card wasn't updated." +
              "###Required user roles\n\n" +
              "* ROLE_ACCESS_CARD_PROCESSING and ROLE_CARD_PROCESSING_SAVE_VOTING_RIGHT to mark the card as used.\n\n" +
              "* ROLE_ACCESS_CARD_PROCESSING and ROLE_CARD_PROCESSING_BLOCK_VOTING_CARD to block a card.",
      consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
  @ApiResponses(value = {
      @ApiResponse(code = 400, message = "If the requested status is neither 'USED' nor 'BLOCKED'"),
      @ApiResponse(code = 403, message = "The current user is not authorized to access this resource"),
      @ApiResponse(code = 404, message = "Voting card not found"),
      @ApiResponse(code = 409, message = "The voting card was found but its voting right status cannot be modified")
  })
  @PreAuthorize("hasRole('ACCESS_CARD_PROCESSING') AND (" +
                "(hasRole('CARD_PROCESSING_SAVE_VOTING_RIGHT') AND #status.name() == 'USED') OR " +
                "(hasRole('CARD_PROCESSING_BLOCK_VOTING_CARD') AND #status.name() == 'BLOCKED'))")
  public void updateVotingRightStatus(
      @ApiParam(value = "The operation id") @PathVariable("operationId") Long operationId,
      @ApiParam(value = "Voter index") @PathVariable("voterIndex") Long voterIndex,
      @ApiParam(value = "The new voting right status") @RequestParam VotingRightStatus status,
      @ApiParam(value = "The voting channel used to cast the vote")
      @RequestParam(required = false) VotingChannel votingChannel
  ) {
    logger.info("updating voting right status with [{}] on channel [{}] for voter index [{}] and operation ID [{}]",
                status,
                votingChannel,
                voterIndex,
                operationId);

    switch (status) {
      case USED:
        votingCardService.markAsUsed(operationId, voterIndex, votingChannel);
        break;
      case BLOCKED:
        votingCardService.markAsBlocked(operationId, voterIndex);
        break;
      default:
        throw new IllegalArgumentException(String.format(
            "The voting right status [%s] cannot be manually applied to a voting card.", status));
    }
  }

  /**
   * Perform a search on {@link ch.ge.ve.vrum.repository.votingcard.entity.VotingCard} by voter index and return all
   * corresponding {@link VotingCardDto}.
   *
   * @param operationId the operation id.
   * @param voterIndex  the voter index.
   *
   * @return all matching voting cards.
   *
   * @throws EntityNotFoundException if no operation was found.
   */
  @GetMapping(params = "voterIndex", produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(
      value = "Retrieve the voting cards",
      notes = "Retrieve a list of voting cards matching the specified voter index" +
              "###Required user roles\n\n" +
              "* ROLE_ACCESS_SEARCH" +
              "* ROLE_SEARCH_SELECT_VOTING_CARD")
  @ApiResponses(value = {
      @ApiResponse(code = 403, message = "The current user is not authorized to access this resource"),
      @ApiResponse(code = 404, message = "Operation not found"),
  })
  @PreAuthorize("hasRole('ACCESS_SEARCH') and hasRole('SEARCH_SELECT_VOTING_CARD')")
  public List<VotingCardDto> findByVoterIndex(
      @ApiParam(value = "The operation id") @PathVariable("operationId") Long operationId,
      @ApiParam(value = "The voter index") @RequestParam("voterIndex") String voterIndex) {
    return votingCardService.findByOperationIdAndVoterIndex(operationId, Long.valueOf(voterIndex))
                            .map(List::of)
                            .orElse(List.of());
  }

  /**
   * Perform a search on {@link ch.ge.ve.vrum.repository.votingcard.entity.VotingCard} by identification code and return
   * all corresponding {@link VotingCardDto}.
   *
   * @param operationId        the operation id.
   * @param identificationCode the identification code.
   *
   * @return all matching voting cards.
   */
  @GetMapping(params = "identificationCode", produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(
      value = "Retrieve the voting cards",
      notes = "Retrieve a list of voting cards matching the specified identification code" +
              "###Required user roles\n\n" +
              "* ROLE_ACCESS_SEARCH" +
              "* ROLE_SEARCH_SELECT_VOTING_CARD")
  @ApiResponses(value = {
      @ApiResponse(code = 403, message = "The current user is not authorized to access this resource"),
      @ApiResponse(code = 404, message = "Operation not found"),
  })
  @PreAuthorize("hasRole('ACCESS_SEARCH') and hasRole('SEARCH_SELECT_VOTING_CARD')")
  public List<VotingCardDto> findByIdentificationCode(
      @ApiParam(value = "The operation id") @PathVariable("operationId") Long operationId,
      @ApiParam(value = "The identification code")
      @RequestParam("identificationCode") String identificationCode) {
    // at the moment we don't know the algorithm that will be used to hash the identification code
    // when we do :
    // FIXME: use hash algorithm to hash the identification code
    return votingCardService.findByOperationIdAndIdentificationCodeHash(operationId, identificationCode);
  }

  /**
   * Perform a search on {@link ch.ge.ve.vrum.repository.votingcard.entity.VotingCard} by identification code hash and
   * return all corresponding {@link VotingCardDto}.
   *
   * @param operationId            the operation id.
   * @param identificationCodeHash the identification code hash.
   *
   * @return all matching voting cards.
   *
   * @throws EntityNotFoundException if no operation was found.
   */
  @GetMapping(params = "identificationCodeHash", produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(
      value = "Retrieve the voting cards",
      notes = "Retrieve a list of voting cards matching the specified identification code hash" +
              "###Required user roles\n\n" +
              "* ROLE_ACCESS_SEARCH" +
              "* ROLE_SEARCH_SELECT_VOTING_CARD")
  @ApiResponses(value = {
      @ApiResponse(code = 403, message = "The current user is not authorized to access this resource"),
      @ApiResponse(code = 404, message = "Operation not found"),
  })
  @PreAuthorize("hasRole('ACCESS_SEARCH') and hasRole('SEARCH_SELECT_VOTING_CARD')")
  public List<VotingCardDto> findByIdentificationCodeHash(
      @ApiParam(value = "The operation id") @PathVariable("operationId") Long operationId,
      @ApiParam(value = "The hash of the identification code")
      @RequestParam("identificationCodeHash") String identificationCodeHash
  ) {
    return votingCardService.findByOperationIdAndIdentificationCodeHash(operationId, identificationCodeHash);
  }

  /**
   * Perform a search on {@link ch.ge.ve.vrum.repository.votingcard.entity.VotingCard} by local person id and return all
   * corresponding {@link VotingCardDto}.
   *
   * @param operationId   the operation id.
   * @param localPersonId the local person id.
   *
   * @return all matching voting cards.
   *
   * @throws EntityNotFoundException if no operation was found.
   */
  @GetMapping(params = "localPersonId", produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(
      value = "Retrieve the voting cards",
      notes = "Retrieve a list of voting cards matching the specified local person id" +
              "###Required user roles\n\n" +
              "* ROLE_ACCESS_SEARCH" +
              "* ROLE_SEARCH_SELECT_VOTING_CARD")
  @ApiResponses(value = {
      @ApiResponse(code = 403, message = "The current user is not authorized to access this resource"),
      @ApiResponse(code = 404, message = "Operation not found"),
  })
  @PreAuthorize("hasRole('ACCESS_SEARCH') and hasRole('SEARCH_SELECT_VOTING_CARD')")
  public List<VotingCardDto> findByLocalPersonId(
      @ApiParam(value = "The operation id") @PathVariable("operationId") Long operationId,
      @ApiParam(value = "The local person id") @RequestParam("localPersonId") String localPersonId
  ) {
    return votingCardService.findByOperationIdAndLocalPersonId(operationId, localPersonId);
  }

}
