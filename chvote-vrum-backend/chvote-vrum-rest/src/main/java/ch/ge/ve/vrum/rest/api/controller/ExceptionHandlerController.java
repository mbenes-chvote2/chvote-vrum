/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.rest.api.controller;

import ch.ge.ve.vrum.repository.votingcard.entity.VotingRightStatus;
import ch.ge.ve.vrum.service.exception.EntityNotFoundException;
import ch.ge.ve.vrum.service.exception.IllegalVotingRightStatusTransitionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * A controller advice containing common exception to HTTP status mapping functionality.
 */
@ControllerAdvice
public class ExceptionHandlerController {

  private static final Logger logger = LoggerFactory.getLogger(VotingCardController.class);

  /**
   * Captures and sets HTTP Status to {@link HttpStatus#NOT_FOUND} for all suitable exceptions.
   *
   * @param ex the exception being handled
   *
   * @see EntityNotFoundException
   */
  @ResponseStatus(value = HttpStatus.NOT_FOUND)
  @ExceptionHandler({EntityNotFoundException.class})
  public void notFound(EntityNotFoundException ex) {
    logger.info("Cannot find the specified resource.", ex);
  }

  /**
   * Captures and sets HTTP Status to {@link HttpStatus#BAD_REQUEST} for all suitable exceptions.
   *
   * @param ex the exception being handled
   *
   * @see IllegalArgumentException
   */
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  @ExceptionHandler({IllegalArgumentException.class})
  public void badRequest(IllegalArgumentException ex) {
    logger.info("Cannot handle the given request.", ex);
  }

  /**
   * Captures and sets HTTP Status to {@link HttpStatus#CONFLICT} for all suitable exceptions.
   *
   * @param ex the exception being handled
   *
   * @see IllegalStateException
   */
  @ResponseStatus(value = HttpStatus.CONFLICT)
  @ExceptionHandler({IllegalStateException.class})
  public void conflict(IllegalStateException ex) {
    logger.info("Encountered a resource in an non-compliant state.", ex);
  }

  /**
   * Captures and sets HTTP Status to {@link HttpStatus#CONFLICT} for
   * {@code IllegalVotingRightStatusTransitionException} and additionally
   * returns the conflicting voting right status.
   *
   * @param ex the exception being handled
   *
   * @return the conflicting voting right status
   *
   * @see IllegalVotingRightStatusTransitionException
   */
  @ResponseBody
  @ResponseStatus(value = HttpStatus.CONFLICT)
  @ExceptionHandler({IllegalVotingRightStatusTransitionException.class})
  public VotingRightStatus votingRightStatusConflict(IllegalVotingRightStatusTransitionException ex) {
    logger.warn(ex.getMessage());

    return ex.getVotingCard().getVotingRightStatus();
  }

}
