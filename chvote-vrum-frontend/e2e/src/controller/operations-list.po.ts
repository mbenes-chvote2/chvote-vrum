/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { browser, by, element, ExpectedConditions } from 'protractor';
import { SidenavController } from './sidenav.po';

export class OperationsListController {

  static get operationsList() {
    return element(by.css('#operationListSection'));
  }

  static get isOperationsListPage() {
    return OperationsListController.operationsList.isPresent();
  }

  private static get sidenavLink() {
    return element(by.css('#vrum-list-operations-menu-link'));
  }

  private static get operationsRows() {
    return element.all(by.css('#operationsGrid mat-row'));
  }

  static getOperationRow(index: number) {
    return this.operationsRows.get(index);
  }

  static get numberOfRows() {
    return OperationsListController.operationsRows.count();
  }

  static open() {
    return SidenavController.openSideNav()
      .then(() => browser.wait(ExpectedConditions.elementToBeClickable(OperationsListController.sidenavLink)))
      .then(() => OperationsListController.sidenavLink.click())
      .then(() => browser.wait(ExpectedConditions.invisibilityOf(SidenavController.overlayElmt)))
      .then(() => browser.wait(ExpectedConditions.presenceOf(OperationsListController.operationsList)));
  }

}
