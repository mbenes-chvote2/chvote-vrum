/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { browser, ExpectedConditions } from 'protractor';
import { LoginController } from '../controller/login.po';
import { OperationsListController } from '../controller/operations-list.po';

export class LoginTestHelper {

  static testValidLogin(username: string, password: string) {
    it('should login and redirect to homepage with valid creds', () => {
      LoginController.login(username, password);
      browser.wait(ExpectedConditions.presenceOf(OperationsListController.operationsList));
      expect(LoginController.isLoginPage).toBeFalsy();
      expect(LoginController.currentUser).toBe(username);
      expect(OperationsListController.isOperationsListPage).toBeTruthy();
    });
  }

  static testLogout() {
    it('should logout and redirect to login form', (done) => {
      LoginController.logout().then(() => {
        expect(LoginController.isLoginPage).toBeTruthy();
        done();
      });
    });
  }

}
