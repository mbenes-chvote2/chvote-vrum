/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { browser, protractor } from 'protractor';
import * as querystring from 'querystring';
import * as http from 'http';
import { RequestOptions } from 'http';
import { isNullOrUndefined, log } from 'util';

export class MockServer {
  private static request(path: string, method = 'GET', body?: any) {
    const deferred = protractor.promise.defer();
    const options: RequestOptions = {
      hostname: browser.params.mockServer.hostname,
      port: browser.params.mockServer.port,
      path: path,
      headers: {},
      method: method
    };
    let postData;

    if (!isNullOrUndefined(body)) {
      postData = querystring.stringify(body);
      options.headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Content-Length': Buffer.byteLength(postData)
      };
    }

    const callback = function (res) {
      let data = '';
      res.on('data', (chunk) => {
        data += chunk;
      });
      res.on('end', () => {
        deferred.fulfill(data);
      });
    };

    const request = http.request(options, callback);

    if (!isNullOrUndefined(postData)) {
      request.write(postData);
    }

    request.end();

    return deferred.promise;
  }

  private static requestWithCallBack(path: string, onResultCallBack: (any) => any, method?: string) {
    browser.wait(() => MockServer.request(path, method)
      .then(data => {
        try {
          onResultCallBack(data);
        } catch (e) {
          return false;
        }
        return true;
      }), 30000);
  }

  static resetDB() {
    log('resetting database');
    return MockServer.request('/mock/database', 'DELETE');
  }

  static createMockData() {
    log('Create mock data');

    return MockServer.request('/mock/database', 'POST')
      .then(() => MockServer.request('/mock/voting-cards', 'POST', {
        nbrOfVotingCards: 10,
        countingCircleBusinessId: '6621',
        doiBusinessId: 'GE',
        protocolId: 'OP_1',
        votingRightStatus: 'AVAILABLE'
      }));
  }
}
