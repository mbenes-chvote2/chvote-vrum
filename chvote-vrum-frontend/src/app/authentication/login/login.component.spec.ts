/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { Router } from '@angular/router';
import { of, throwError } from 'rxjs';
import { AuthenticationService } from '../../core/authentication/authentication.service';
import { BaseUrlService } from '../../core/base-url/base-url.service';
import { MaterialModule } from '../../material/material.module';

import { LoginComponent } from './login.component';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let baseUrlServiceSpy;
  let authenticationServiceSpy;
  let routerSpy;

  beforeEach(async(() => {
    baseUrlServiceSpy = jasmine.createSpy('BaseUrlService');
    authenticationServiceSpy = jasmine.createSpyObj('AuthenticationService', ['login', 'logout']);
    routerSpy = jasmine.createSpyObj('Router', ['navigate']);

    TestBed.configureTestingModule({
      imports: [MaterialModule, NoopAnimationsModule, FormsModule, HttpClientModule],
      declarations: [LoginComponent],
      providers: [
        LoginComponent,
        {provide: BaseUrlService, useValue: baseUrlServiceSpy},
        {provide: AuthenticationService, useValue: authenticationServiceSpy},
        {provide: Router, useValue: routerSpy}
      ]
    })
      .compileComponents();
  }));

  beforeEach(async(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', async(() => {
    expect(component).toBeTruthy();
  }));

  it('should log in and redirect to root', async(() => {
    authenticationServiceSpy.login.and.returnValue(of(true));

    component.login({username: 'username', password: 'password'});

    expect(authenticationServiceSpy.login).toHaveBeenCalledWith('username', 'password');
    expect(routerSpy.navigate).toHaveBeenCalledWith(['/']);
  }));

  it('should fail at log in and then logout', async(() => {
    authenticationServiceSpy.login.and.returnValue(throwError('this is an error'));

    component.login({username: 'username', password: 'password'});

    expect(authenticationServiceSpy.login).toHaveBeenCalled();
    expect(routerSpy.navigate).not.toHaveBeenCalled();
    expect(authenticationServiceSpy.logout).toHaveBeenCalled();
  }));
});
