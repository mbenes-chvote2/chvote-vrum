/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { Router } from '@angular/router';
import { AuthorizationService } from '../../core/authorization/authorization.service';
import { Operation } from '../../core/operations/operation.model';

import { OperationsListComponent } from './operations-list.component';

describe('OperationsListComponent', () => {
  const expectedOperations = [
    <Operation>{id: 1, operationDate: new Date(), shortLabel: 'Operation 1', longLabel: 'Long label operation 1'},
    <Operation>{id: 2, operationDate: new Date(), shortLabel: 'Operation 2', longLabel: 'Long label operation 2'},
    <Operation>{id: 3, operationDate: new Date(), shortLabel: 'Operation 3', longLabel: 'Long label operation 3'},
    <Operation>{id: 4, operationDate: new Date(), shortLabel: 'Operation 4', longLabel: 'Long label operation 4'}
  ];

  let routerSpy: jasmine.SpyObj<Router>;
  const activatedRouteMock = {snapshot: {data: {operations: expectedOperations}}};
  let component: OperationsListComponent;

  beforeEach(() => {
    routerSpy = jasmine.createSpyObj('Router', ['navigate']);
    component = new OperationsListComponent(<any> routerSpy, <any> activatedRouteMock);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('#ngOnInit should define dataSource', () => {
    component.ngOnInit();

    expect(component.dataSource).toBeDefined();
    expect(component.dataSource).not.toBeNull();
  });

  it('#onOperationSelected should navigate to a selected operation', () => {
    const idStub = 1;

    component.onOperationSelected(idStub);

    expect(routerSpy.navigate).toHaveBeenCalledTimes(1);
    expect(routerSpy.navigate).toHaveBeenCalledWith(['/operations', idStub, 'card-processing']);
  });
});
