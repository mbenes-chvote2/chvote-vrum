/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MediaChange, ObservableMedia } from '@angular/flex-layout';
import { MatSidenav } from '@angular/material';
import { Subscription } from 'rxjs';

import { isNullOrUndefined } from 'util';

import { AuthorizationService } from '../../core/authorization/authorization.service';
import { User } from '../../core/authorization/user.model';
import { LoadingSpinnerService } from '../../core/http/loading-spinner.service';
import { Version } from '../../core/version/version.model';
import { VersionService } from '../../core/version/version.service';

@Component({
  selector: 'vrum-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit, OnDestroy {

  @ViewChild(MatSidenav)
  sidenav: MatSidenav;

  sidenavMode = 'side';
  sidenavOpened = false;

  private _version: Version;
  private user: User;
  private _loading = false;
  private _loadingSpinnerSubscription: Subscription;

  constructor(private authorisationService: AuthorizationService,
              private versionService: VersionService,
              private media: ObservableMedia,
              private loadingSpinnerService: LoadingSpinnerService) {
  }

  ngOnInit(): void {
    this.media.subscribe((mediaChange: MediaChange) => {
      this.sidenavMode = this.getMode(mediaChange);
      this.sidenavOpened = this.getOpened(mediaChange);
    });

    this.authorisationService.userChange.subscribe(user => {
      this.user = user;

      if (user) {
        if (this.sidenavMode === 'side') {
          this.sidenavOpened = true;
        }

        if (!this._version) {
          this.versionService.get().subscribe(version => this._version = version);
        }
      } else {
        this.sidenavOpened = false;
        this.closeSideNav();
      }
    });

    this._loadingSpinnerSubscription = this.loadingSpinnerService.loadingEvents
      .subscribe(loading => this._loading = loading);
  }

  ngOnDestroy(): void {
    this._loadingSpinnerSubscription.unsubscribe();
  }

  get version() {
    return this._version;
  }

  get isLoading() {
    return this._loading;
  }

  closeSideNav() {
    if (!this.media.isActive('gt-md')) {
      this.sidenav.close();
    }
  }

  toggle() {
    if (!this.media.isActive('gt-md')) {
      this.sidenav.toggle();
    }
  }

  // set mode based on a breakpoint
  private getMode(mediaChange: MediaChange): string {
    if (this.media.isActive('gt-md')) {
      return 'side';
    } else {
      return 'over';
    }
  }

  // open/close as needed
  private getOpened(mediaChange: MediaChange): boolean {
    if (this.media.isActive('gt-md')) {
      return !isNullOrUndefined(this.user);
    } else {
      return false;
    }
  }

}
