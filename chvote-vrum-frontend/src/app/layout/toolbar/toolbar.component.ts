/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { AuthenticationService } from '../../core/authentication/authentication.service';
import { AuthorizationService } from '../../core/authorization/authorization.service';
import { User } from '../../core/authorization/user.model';
import { SidenavComponent } from '../sidenav/sidenav.component';

@Component({
  selector: 'vrum-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {
  @Input('sidenav')
  sidenav: SidenavComponent;

  private user: User;

  constructor(private translate: TranslateService, private router: Router,
              private authenticationService: AuthenticationService,
              private authorisationService: AuthorizationService) {
  }

  ngOnInit(): void {
    this.authorisationService.userChange.subscribe(user => {
      this.user = user;
    });
  }

  get currentUser() {
    return this.user;
  }

  get currentLang(): string {
    return this.translate.currentLang;
  }

  set currentLang(value: string) {
    this.translate.use(value);
  }

  toggleSidenav() {
    this.sidenav.toggle();
  }

  logout(): void {
    this.authenticationService.logout();
    this.router.navigate(['login']);
  }

  availableLangs(): Array<string> {
    return this.translate.getLangs();
  }

}
