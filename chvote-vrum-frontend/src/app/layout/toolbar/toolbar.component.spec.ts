/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { of } from 'rxjs';
import { AuthenticationService } from '../../core/authentication/authentication.service';
import { AuthorizationService } from '../../core/authorization/authorization.service';
import { User } from '../../core/authorization/user.model';
import { ToolbarComponent } from './toolbar.component';

describe('ToolbarComponent', () => {
  let toolbarComponent: ToolbarComponent;
  const availableLangs = ['fr', 'de'];
  const defaultLang = 'fr';
  let currentLang = defaultLang;

  const translateServiceMock: TranslateService = <any> {
    use: jasmine.createSpy('use').and.callFake((lang: string) => {
      currentLang = lang;
    }),
    get currentLang(): string {
      return currentLang;
    },
    getLangs: () => availableLangs,
    getDefaultLang: () => defaultLang,
    getBrowserLang: () => availableLangs[0]
  };

  const routerMock: Router = <any> {
    navigate: jasmine.createSpy('navigate'),
  };

  const authenticationServiceMock: AuthenticationService = <any> {};

  const authorizationServiceMock: AuthorizationService = <any> {
    currentUser: of(<User> {
      login: 'login',
      roles: ['USER'],
      managementEntity: 'GE'
    })
  };

  beforeEach(function () {
    toolbarComponent = new ToolbarComponent(translateServiceMock, routerMock, authenticationServiceMock, authorizationServiceMock);
  });

  it('should have FR and DE translations', () => {
    expect(toolbarComponent.availableLangs()).toContain('fr');
    expect(toolbarComponent.availableLangs()).toContain('de');
  });

  it('should set the correct language on translate service when changing language', () => {
    // when
    toolbarComponent.currentLang = 'de';
    // then
    expect(translateServiceMock.use).toHaveBeenCalledWith('de');
    expect(toolbarComponent.currentLang).toBe('de');
  });
});
