/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateService } from '@ngx-translate/core';
import { AppComponent } from './app.component';
import { BaseUrlService } from './core/base-url/base-url.service';
import { LayoutModule } from './layout/layout.module';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let translateServiceSpy;
  let baseUrlServiceSpy;

  beforeEach(async(() => {
    translateServiceSpy = jasmine.createSpyObj('TranslateService',
      ['addLangs', 'getBrowserLang', 'getDefaultLang', 'setDefaultLang', 'use']);

    baseUrlServiceSpy = jasmine.createSpy('BaseUrlService');

    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        RouterTestingModule,
        LayoutModule
      ],
      providers: [
        AppComponent,
        {provide: TranslateService, useValue: translateServiceSpy},
        {provide: BaseUrlService, useValue: baseUrlServiceSpy}
      ],
      declarations: [AppComponent]
    }).compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(AppComponent);
      });
  }));

  it('should create the app', async(() => {
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('should initialize TranslateService with BrowserLang', async(() => {
    const app = fixture.debugElement.componentInstance;
    const translateService = fixture.debugElement.injector.get(TranslateService);

    translateServiceSpy.getBrowserLang.and.returnValue('fr');

    app.ngOnInit();
    expect(translateService.addLangs).toHaveBeenCalledWith(['fr', 'de']);
    expect(translateService.setDefaultLang).toHaveBeenCalledWith('fr');
    expect(translateService.getBrowserLang).toHaveBeenCalled();
    expect(translateService.use).toHaveBeenCalledWith('fr');
    expect(translateService.getDefaultLang).not.toHaveBeenCalled();
  }));

  it('should initialize TranslateService with DefaultLang', async(() => {
    const app = fixture.debugElement.componentInstance;
    const translateService = fixture.debugElement.injector.get(TranslateService);

    translateServiceSpy.getBrowserLang.and.returnValue('en');
    translateServiceSpy.getDefaultLang.and.returnValue('default');

    app.ngOnInit();
    expect(translateService.addLangs).toHaveBeenCalledWith(['fr', 'de']);
    expect(translateService.setDefaultLang).toHaveBeenCalledWith('fr');
    expect(translateService.getBrowserLang).toHaveBeenCalled();
    expect(translateService.getDefaultLang).toHaveBeenCalled();
    expect(translateService.use).toHaveBeenCalledWith('default');
  }));

  it('should render toolbar', async(() => {
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('vrum-toolbar')).toBeTruthy();
  }));

  it('should render sidenav', async(() => {
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('vrum-sidenav')).toBeTruthy();
  }));

  it('should render vrum-content inside sidenav', async(() => {
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('vrum-sidenav div.vrum-content')).toBeTruthy();
  }));
});
