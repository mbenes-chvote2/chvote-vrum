/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { APP_BASE_HREF } from '@angular/common';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute, RouterModule } from '@angular/router';

import { of } from 'rxjs';
import { AuthorizationService } from '../../core/authorization/authorization.service';

import { BaseUrlService } from '../../core/base-url/base-url.service';
import { CoreModule } from '../../core/core.module';
import { MaterialModule } from '../../material/material.module';
import { SharedModule } from '../../shared/shared.module';
import { SearchComponent } from './search.component';

class MockActivatedRoute {
  parent: any;
  snapshot = {
    params: {}
  };

  constructor(options) {
    this.parent = options.parent;
    this.snapshot.params = options.params;
  }
}

describe('SearchComponent', () => {

  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;
  let votingCardsServiceSpy;
  let configurationServiceSpy;
  let authorizationServiceSpy;
  const activatedRouteMock = <any> new MockActivatedRoute({
    parent: new MockActivatedRoute({
      params: of({id: '1'})
    })
  });

  const baseUrlServiceMock = jasmine.createSpy('BaseUrlService');

  beforeEach(async(() => {
    votingCardsServiceSpy = jasmine.createSpyObj('VotingCardsService', ['searchByIdentifier']);
    configurationServiceSpy = jasmine.createSpyObj('ConfigurationService',
      ['getIdentifierTypes', 'getDefaultIdentifierType']);
    authorizationServiceSpy = jasmine.createSpyObj('AuthorizationService',
      ['hasAtLeastOneRole']);

    TestBed.configureTestingModule({
      imports: [
        CoreModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        NoopAnimationsModule,
        RouterModule.forRoot([]),
        SharedModule
      ],
      providers: [
        SearchComponent,
        {provide: BaseUrlService, useValue: baseUrlServiceMock},
        {provide: ActivatedRoute, useValue: activatedRouteMock},
        {provide: BaseUrlService, useValue: baseUrlServiceMock},
        {provide: APP_BASE_HREF, useValue: '/'},
        {provide: AuthorizationService, useValue: authorizationServiceSpy}
      ],
      declarations: [SearchComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
