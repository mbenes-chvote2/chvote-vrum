/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { Injectable } from '@angular/core';

import { AuthorizationService } from '../authorization/authorization.service';

export const AUTHORIZATION_KEY = 'authorization-vrum';

@Injectable({providedIn: 'root'})
export class AuthenticationService {
  canLogout = false;

  constructor(private authorizationService: AuthorizationService) {
    this.canLogout = !!localStorage.getItem(AUTHORIZATION_KEY);
  }

  login(username: string, password: string) {
    localStorage.setItem(AUTHORIZATION_KEY, btoa(`${username}:${password}`));
    this.canLogout = true;
    return this.authorizationService.updateUser();
  }

  logout() {
    localStorage.removeItem(AUTHORIZATION_KEY);
    this.authorizationService.clear();
    this.canLogout = false;
  }
}
