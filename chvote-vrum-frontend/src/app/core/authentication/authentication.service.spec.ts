/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { AuthenticationService } from './authentication.service';

describe('AuthenticationService', () => {
  let authorizationServiceSpy;
  let service: AuthenticationService;

  beforeEach(() => {
    authorizationServiceSpy = jasmine.createSpyObj('AuthorizationService', ['updateUser', 'clear']);

    service = new AuthenticationService(authorizationServiceSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should updateUser at login', () => {
    service.login('username', 'password');
    expect(authorizationServiceSpy.updateUser).toHaveBeenCalled();
  });

  it('should clear at logout', () => {
    service.logout();
    expect(authorizationServiceSpy.clear).toHaveBeenCalled();
  });
});
