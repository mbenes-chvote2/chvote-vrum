/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { MatProgressSpinnerModule } from '@angular/material';
import { MissingTranslationHandler, TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AuthenticationInterceptor } from './authentication/authentication.interceptor';
import { AuthorizationGuard } from './authorization/authorization.guard';
import { CheckRolesDirective } from './authorization/check-roles.directive';
import { BaseUrlService } from './base-url/base-url.service';
import { CustomMissingTranslationHandler } from './custom-missing-translation-handler';
import { InProgressHttpInterceptor } from './http/in-progress-http-interceptor.service';

// AoT requires an exported function for factories
export function TranslateHttpLoaderFactory(httpClient: HttpClient, baseUrlService: BaseUrlService) {
  return new TranslateHttpLoader(httpClient, baseUrlService.frontendBaseUrl + 'assets/i18n/');
}

@NgModule({
  declarations: [
    CheckRolesDirective
  ],
  exports: [
    TranslateModule,
    CheckRolesDirective
  ],
  imports: [
    HttpClientModule,
    MatProgressSpinnerModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: TranslateHttpLoaderFactory,
        deps: [HttpClient, BaseUrlService]
      },
      missingTranslationHandler: {provide: MissingTranslationHandler, useClass: CustomMissingTranslationHandler}
    })
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AuthenticationInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: InProgressHttpInterceptor, multi: true},
    AuthorizationGuard
  ],
})
export class CoreModule {
}
