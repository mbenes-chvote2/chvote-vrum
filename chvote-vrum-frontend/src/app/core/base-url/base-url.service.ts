/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { DOCUMENT } from '@angular/common';
import { Inject, Injectable } from '@angular/core';

@Injectable({providedIn: 'root'})
export class BaseUrlService {
  private readonly _frontendBaseUrl;
  private readonly _backendBaseUrl;

  constructor(@Inject(DOCUMENT) private document: any) {
    this._frontendBaseUrl = document.getElementsByTagName('base')[0].href;
    this._backendBaseUrl = document.querySelector('meta[name="apiBaseUrl"]').getAttribute('content');
  }

  get frontendBaseUrl() {
    return this._frontendBaseUrl;
  }

  get backendBaseUrl() {
    return this._backendBaseUrl;
  }
}
