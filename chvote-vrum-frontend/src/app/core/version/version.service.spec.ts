/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { HttpClient } from '@angular/common/http';
import { BaseUrlService } from '../base-url/base-url.service';

import { VersionService } from './version.service';

describe('VersionService', () => {
  let httpClientSpy;
  let service: VersionService;

  beforeEach(() => {
    const document = <any> {
      getElementsByTagName: (name: String) => {
        return (name === 'base') ? [{href: ''}] : [];
      },
      querySelector: (selector: String) => {
        return (selector === 'meta[name="apiBaseUrl"]') ? {
          getAttribute: (attribute: String) => {
            return (attribute === 'content') ? 'api' : undefined;
          }
        } : undefined;
      }
    };
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);

    service = new VersionService(httpClientSpy, new BaseUrlService(document));
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call version', () => {
    service.get();
    expect(httpClientSpy.get).toHaveBeenCalledWith('api/version');
  });
});
