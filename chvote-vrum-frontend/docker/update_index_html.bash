#!/bin/bash

#---------------------------------------------------------------------------------------------------
# - #%L                                                                                            -
# - chvote-vrum                                                                                    -
# - %%                                                                                             -
# - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
# - %%                                                                                             -
# - This program is free software: you can redistribute it and/or modify                           -
# - it under the terms of the GNU Affero General Public License as published by                    -
# - the Free Software Foundation, either version 3 of the License, or                              -
# - (at your option) any later version.                                                            -
# -                                                                                                -
# - This program is distributed in the hope that it will be useful,                                -
# - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
# - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
# - GNU General Public License for more details.                                                   -
# -                                                                                                -
# - You should have received a copy of the GNU Affero General Public License                       -
# - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
# - #L%                                                                                            -
#---------------------------------------------------------------------------------------------------

echo "update_index_html - START"
set -e
set -o errexit
set -o nounset
set -o pipefail
#set -o xtrace

# replace the base href with BASE_URL environment variable if set, with / otherwise
sed -i "s#<base href=\"\/\">#<base href=\"${BASE_URL:-/}\">#g" /app/index.html
sed -i "s#<meta name=\"apiBaseUrl\" content=\"http://localhost:8381/api\">#<meta name=\"apiBaseUrl\" content=\"${BASE_URL:-}api\">#g" /app/index.html

# nginx -g "daemon off;"

. /liblog.sh
. /libnginx.sh

info "Starting nginx... "

exec "$NGINX_BASEDIR/sbin/nginx" -c "$NGINX_CONFDIR/nginx.conf" -g "daemon off;"
