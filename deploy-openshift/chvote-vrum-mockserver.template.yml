#---------------------------------------------------------------------------------------------------
# - #%L                                                                                            -
# - chvote-vrum                                                                                    -
# - %%                                                                                             -
# - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
# - %%                                                                                             -
# - This program is free software: you can redistribute it and/or modify                           -
# - it under the terms of the GNU Affero General Public License as published by                    -
# - the Free Software Foundation, either version 3 of the License, or                              -
# - (at your option) any later version.                                                            -
# -                                                                                                -
# - This program is distributed in the hope that it will be useful,                                -
# - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
# - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
# - GNU General Public License for more details.                                                   -
# -                                                                                                -
# - You should have received a copy of the GNU Affero General Public License                       -
# - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
# - #L%                                                                                            -
#---------------------------------------------------------------------------------------------------

apiVersion: v1
kind: Template
labels:
  app: VRUM
  role: mock-server
metadata:
  annotations:
    description: VRUM Mock Server deployment template
    iconClass: icon-nodejs
    openshift.io/display-name: VRUM Mock Server deployment template
    openshift.io/long-description: VRUM Mck Server deployment template
    tags: VRUM
    template.openshift.io/bindable: "false"
  name: chvote-vrum-mockserver-template
parameters:
- displayName: VRUM mock server image name
  name: VRUM_MOCKSERVER_IMAGE
  required: true
  value: chvote-vrum-mock-server
- displayName: VRUM mock server docker image tag
  name: VRUM_MOCKSERVER_IMAGE_TAG
  required: true
  value: latest
- description: Minimum amount of memory the pod can use.
  displayName: Minimum Memory allocation
  name: VRUM_MOCKSERVER_MEMORY_REQUEST
  required: true
  value: 500M
- description: Maximum amount of memory the pod can use.
  displayName: Memory Limit
  name: VRUM_MOCKSERVER_MEMORY_LIMIT
  required: true
  value: 700M
- description: Minimum amount of CPU the pod can use.
  displayName: CPU Request
  name: VRUM_MOCKSERVER_CPU_REQUEST
  required: true
  value: 250m
- description: Maximum amount of CPU the pod can use.
  displayName: CPU Limit
  name: VRUM_MOCKSERVER_CPU_LIMIT
  required: true
  value: 1000m
objects:
- apiVersion: v1
  kind: Service
  metadata:
    name: vrum-mockserver-api
    annotations:
      description: Exposes the VRUM Mock Server api
  spec:
    ports:
    - name: api
      port: 48646
      targetPort: 48646
    selector:
      app: VRUM
      role: mock-server
- apiVersion: v1
  kind: ServiceAccount
  metadata:
    name: vrum-mockserver-sa
    labels:
      app: VRUM
      role: mock-server
- apiVersion: v1
  kind: ImageStream
  metadata:
    name: ${VRUM_MOCKSERVER_IMAGE}-is
    labels:
      app: VRUM
      role: mock-server
  spec:
    lookupPolicy:
      local: false
    tags:
    - name: ${VRUM_MOCKSERVER_IMAGE_TAG}
      from:
        kind: ImageStreamTag
        name: ${VRUM_MOCKSERVER_IMAGE}:${VRUM_MOCKSERVER_IMAGE_TAG}
        namespace: chvote-images
- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    name: vrum-mockserver-dc
    annotations:
      description: Defines how to deploy the VRUM Mock Server application
      template.alpha.openshift.io/wait-for-ready: "true"
    labels:
      app: VRUM
      role: mock-server
  spec:
    replicas: 1
    selector:
      name: vrum-mockserver-container
    strategy:
      type: Recreate
    triggers:
    - imageChangeParams:
        automatic: true
        containerNames:
        - vrum-mockserver-container
        from:
          kind: ImageStreamTag
          name: ${VRUM_MOCKSERVER_IMAGE}-is:${VRUM_MOCKSERVER_IMAGE_TAG}
      type: ImageChange
    - type: ConfigChange
    template:
      metadata:
        name: vrum-mockserver-container-template
        labels:
          app: VRUM
          name: vrum-mockserver-container
          role: mock-server
      spec:
        containers:
        - name: vrum-mockserver-container
          env:
          - name: LOG_DIR
            value: /log
          - name: JAVA_OPTS
            value: -Xmx400m
          - name: SPRING_DATASOURCE_URL
            value: jdbc:h2:tcp://vrum-backend-db:48645/mem:test;MODE=ORACLE
          - name: SWAGGER_PATH_MAPPING
            value: /vrum/mock
          image: ${VRUM_MOCKSERVER_IMAGE}-is:${VRUM_MOCKSERVER_IMAGE_TAG}
          ports:
          - containerPort: 48646
          resources:
            limits:
              cpu: ${VRUM_MOCKSERVER_CPU_LIMIT}
              memory: ${VRUM_MOCKSERVER_MEMORY_LIMIT}
            requests:
              cpu: ${VRUM_MOCKSERVER_CPU_REQUEST}
              memory: ${VRUM_MOCKSERVER_MEMORY_REQUEST}
          volumeMounts:
          - mountPath: /log
            name: logs
          livenessProbe:
            failureThreshold: 3
            httpGet:
              path: /mock/swagger-ui.html
              port: 48646
              scheme: HTTP
            initialDelaySeconds: 60
            periodSeconds: 60
            successThreshold: 1
            timeoutSeconds: 30
          readinessProbe:
            failureThreshold: 3
            httpGet:
              path: /mock/swagger-ui.html
              port: 48646
              scheme: HTTP
            initialDelaySeconds: 60
            periodSeconds: 60
            successThreshold: 1
            timeoutSeconds: 30
        serviceAccount: vrum-mockserver-sa
        volumes:
        - emptyDir: {}
          name: logs